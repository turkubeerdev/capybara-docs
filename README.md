# capybara docs

--------

![https://i.imgur.com/0EFAdRS.png](https://i.imgur.com/0EFAdRS.png)

### Overview

This project contains three major parts. 

1. The python client app ([Capybara_Client](https://bitbucket.org/turkubeerdev/capybara-client))
2. The Laravel IOT backend ([Capybara_Backend](https://bitbucket.org/turkubeerdev/capybara_backend))
3. The Laravel admin panel ([Capybara_Admin](https://bitbucket.org/turkubeerdev/capybara_admin))

The first two are key components of this project. They were built and designed to meet the clients specifications. The third part of the project is a front end application designed to showcase how these parts can be used together.

### Support 

To get this project running a few support repos were created. 

1. [Server Scripts](https://bitbucket.org/turkubeerdev/laravel-serve)
2. [Custom PiBakery Blocks and Recipie](https://bitbucket.org/turkubeerdev/pibakery-blocks)